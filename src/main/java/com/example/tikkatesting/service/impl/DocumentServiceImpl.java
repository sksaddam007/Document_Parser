package com.example.tikkatesting.service.impl;

import com.example.tikkatesting.entity.Document;
import com.example.tikkatesting.repository.DocumentRepository;
import com.example.tikkatesting.service.DocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;


@Service
@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
public class DocumentServiceImpl implements DocumentService {
    @Autowired
    DocumentRepository documentRepository;

    @Override
    @Transactional(readOnly = false)
    public Document save(Document document){
            return documentRepository.save(document);
    }

    @Override
    public Document findOne(String docId){
        return  documentRepository.findOne(docId);
    }

    @Override
    @Transactional(readOnly = false)
    public Boolean updateStatus(List<String> ids)
    {
        if(ids != null) {
            Document doc;
            System.out.println(ids);
            for (String temp : ids) {
                doc = documentRepository.findOne(temp);
                if (doc != null) {
                    doc.setStructured(true);
                    doc.setUpdatedAt(new Timestamp(System.currentTimeMillis()));
                    documentRepository.save(doc);
                }
            }
            return true;
        }
        else
        {
            return false;
        }

    }

    @Override
    public List<Document> getAllUnStructuredDocuments(int size){
            List<Document> documentList = (List) documentRepository.findAll();
            documentList = documentList.stream().filter(document -> !document.isStructured()).collect(Collectors.toList());
            if (documentList.size() <= size) {
                return documentList;
            } else
                return documentList.subList(0, size);

    }

    @Override
    public Document getByDocumentName(String documentName){
        return documentRepository.findByDocumentName(documentName);
    }

    @Override
    public Boolean checkIfExistsByName(String documentName){
        if(getByDocumentName(documentName)!=null)
        {
            return true;
        }
        else
            return false;
    }
}
