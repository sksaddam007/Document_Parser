package com.example.tikkatesting.service;

import com.example.tikkatesting.entity.Document;

import java.util.List;

public interface DocumentService {
    Document save(Document document);
    Document findOne(String docId);
    Boolean updateStatus(List<String> ids);
    List<Document> getAllUnStructuredDocuments(int size);
    Document getByDocumentName(String documentName);
    Boolean checkIfExistsByName(String documentName);
}
