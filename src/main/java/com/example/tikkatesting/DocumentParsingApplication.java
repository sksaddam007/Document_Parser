package com.example.tikkatesting;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class DocumentParsingApplication {
	public static void main(String[] args) {
		SpringApplication.run(DocumentParsingApplication.class, args);
	}
}
