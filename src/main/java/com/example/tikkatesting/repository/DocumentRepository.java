package com.example.tikkatesting.repository;

import com.example.tikkatesting.entity.Document;
import org.springframework.data.repository.CrudRepository;

public interface DocumentRepository extends CrudRepository<Document,String> {
    Document findByDocumentName(String name);
}
