package com.example.tikkatesting.entity;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import javax.print.attribute.standard.DocumentName;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name=Document.DOCUMENTS)
public class Document {
    public static final String DOCUMENTS="DOCUMENT";
    private static final String ID_COLUMN="ID";

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = Document.ID_COLUMN)
    @NotNull
    private String documentId;
    private String documentName;
    @NotEmpty
    private String location;
    @NotNull
    private boolean structured;
    @CreatedDate @NotNull
    private Date createdAt;

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    public String getDocumentName() {
        return documentName;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public boolean isStructured() {
        return structured;
    }

    public void setStructured(boolean structured) {
        this.structured = structured;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @LastModifiedDate @NotNull
    private Date updatedAt;


}
