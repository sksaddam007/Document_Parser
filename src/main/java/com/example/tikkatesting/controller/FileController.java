package com.example.tikkatesting.controller;

import com.example.tikkatesting.dto.DocumentDTO;
import com.example.tikkatesting.entity.Document;
import com.example.tikkatesting.service.DocumentService;
import org.apache.commons.io.FilenameUtils;
import org.apache.tika.Tika;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.apache.tika.sax.BodyContentHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.xml.sax.SAXException;
import java.io.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping("/document")
public class FileController {

    @Autowired
    DocumentService documentService;

    @Value("${file.tika.body-content.limit}")
    private int tikka_doc_limit;

    @Value("${file.upload.path}")
    private String file_upload_path;

    FileInputStream fip = null;
    FileOutputStream fop = null;

    @RequestMapping(value = "/doUpload", method = RequestMethod.POST)
    @ResponseBody
    public String upload(@RequestParam MultipartFile file) throws IOException,TikaException,SAXException {
        if (!file.isEmpty()) {

            String fileName = file.getOriginalFilename();
            Parser parser = new AutoDetectParser();
            BodyContentHandler handler = new BodyContentHandler(tikka_doc_limit);
            Metadata metadata = new Metadata();
            ParseContext context = new ParseContext();
            parser.parse(file.getInputStream(), handler, metadata, context);
            Tika tika = new Tika();
            String filetype = tika.detect(fileName);
            String filecontent = tika.parseToString(file.getInputStream());
            String[] metadataNames = metadata.names();
            fop = new FileOutputStream(file_upload_path+FilenameUtils.getBaseName(fileName)+".txt");
            byte[] contentInBytes = handler.toString().getBytes();
            fop.write(contentInBytes);
            fop.flush();
            fop.close();
            Document doc = new Document();
            doc.setDocumentName(FilenameUtils.getBaseName(fileName));
            doc.setLocation(file_upload_path+FilenameUtils.getBaseName(fileName)+".txt");
            doc.setStructured(false);
            doc.setCreatedAt(new Timestamp(System.currentTimeMillis()));
            doc.setUpdatedAt(new Timestamp(System.currentTimeMillis()));
            if(documentService.checkIfExistsByName(doc.getDocumentName()))
            {
                doc.setDocumentId(documentService.getByDocumentName(doc.getDocumentName()).getDocumentId());
                doc.setCreatedAt(documentService.getByDocumentName(doc.getDocumentName()).getCreatedAt());
                documentService.save(doc);
            }
            else
            {
                documentService.save(doc);
            }
            return "The file is successfully uploaded";

        } else {

            return "upload failed";
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "")
    public ResponseEntity<?> getOne(@Param("docId") String docId) throws IOException{
        Document document = documentService.findOne(docId);
        if (document == null)
        {
            return new ResponseEntity<>("cannot find the document", HttpStatus.NOT_ACCEPTABLE);
        }
        else {
            fip = new FileInputStream(document.getLocation());
            return new ResponseEntity<>(readFromInputStream(fip), HttpStatus.OK);
        }

    }
    private String readFromInputStream(InputStream inputStream)
            throws IOException {
        StringBuilder resultStringBuilder = new StringBuilder();
        try (BufferedReader br
                     = new BufferedReader(new InputStreamReader(inputStream))) {
            String line;
            while ((line = br.readLine()) != null) {
                resultStringBuilder.append(line).append("\n");
            }
        }
        return resultStringBuilder.toString();
    }

    @RequestMapping(method = RequestMethod.PUT,value = "/status")
    public ResponseEntity<?> updateDocument(@RequestBody List<String> documentIds) {
        return new ResponseEntity<>(documentService.updateStatus(documentIds),HttpStatus.OK);
    }


    @RequestMapping(method = RequestMethod.GET,value = "",params = "size")
    public List<?> listOfUnstructuredDocuments(@RequestParam int size) throws IOException
    {
        if (size <0)
        {
            return null;
        }
        else {
            List<Document> documentList = documentService.getAllUnStructuredDocuments(size);
            List<DocumentDTO> documentDTOList = new ArrayList<>();
            for (Document element : documentList) {
                FileInputStream file = new FileInputStream(element.getLocation());
                DocumentDTO document = new DocumentDTO(element.getDocumentId(), element.getDocumentName(), readFromInputStream(file), element.isStructured());
                documentDTOList.add(document);
            }
            return documentDTOList;
        }
    }
}
