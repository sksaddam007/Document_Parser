package com.example.tikkatesting.dto;

public class DocumentDTO {
    private String DocumentId;
    private String DocumentName;
    private String RawText;
    private boolean Structured;

    public DocumentDTO(String documentId, String documentName, String rawText, boolean structured) {
        DocumentId = documentId;
        DocumentName = documentName;
        RawText = rawText;
        Structured = structured;
    }

    public String getDocumentId() {
        return DocumentId;
    }

    public void setDocumentId(String documentId) {
        DocumentId = documentId;
    }

    public String getDocumentName() {
        return DocumentName;
    }

    public void setDocumentName(String documentName) {
        DocumentName = documentName;
    }

    public String getRawText() {
        return RawText;
    }

    public void setRawText(String rawText) {
        RawText = rawText;
    }

    public boolean isStructured() {
        return Structured;
    }

    public void setStructured(boolean structured) {
        Structured = structured;
    }
}
